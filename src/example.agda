module example where

open import Data.Product using (_×_)
open import Data.Sum using (_⊎_)
open import Data.Empty using (⊥)
open import Level using (0ℓ)

Pred : Set → Set₁
Pred A = A → Set 

infix 3 ¬_
¬_ : Set → Set
¬ A = A → ⊥

infix 4 _∈_ _∉_

_∈_ : {A : Set} → A → Pred A → Set
x ∈ P = P x

_∉_ : {A : Set} → A → Pred A → Set
x ∉ P = ¬ x ∈ P

_ᶜ : {A : Set} → Pred A → Pred A
P ᶜ = _∉ P

_∪_ : {A : Set} → Pred A → Pred A → Pred A
P ∪ Q = λ x → P x ⊎ Q x

_∩_ : {A : Set} → Pred A → Pred A → Pred A
P ∩ Q = λ x → P x × Q x

_⊆_ : {A : Set} → Pred A → Pred A → Set _
P ⊆ Q = ∀ {x} → x ∈ P → x ∈ Q

_≈_ : {A : Set} → Pred A → Pred A → Set
P ≈ Q = P ⊆ Q × Q ⊆ P

-- ∪-demorgan : {ℓ₁ ℓ₂ : Level} {C : Set } {A : Pred C ℓ₁} {B : Pred C ℓ₂} → 
--     ( ( A ∪ B ) ᶜ ) ≐ ( ( A ᶜ ) ∩ ( B ᶜ) )
-- ∪-demorgan = 
--     ( λ x∈[A∪B]ᶜ →   ( λ x∈A  → contradiction (inj₁ x∈A) x∈[A∪B]ᶜ )
--                    , ( λ x∈B →  contradiction (inj₂ x∈B) x∈[A∪B]ᶜ ) ) 
--   , ( λ ( x∈Aᶜ , x∈Bᶜ ) → (λ x∈[A∪B] →  contradiction₂ x∈[A∪B] x∈Aᶜ x∈Bᶜ) )
