{
  description = "A basic flake for a presentation project";
  nixConfig = {
    bash-prompt = "λ ";
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};

      agda-stdlib = pkgs.agdaPackages.standard-library.overrideAttrs (oldAttrs: {
          version = "2.0";
          src = pkgs.fetchFromGitHub {
            repo = "agda-stdlib";
            owner = "agda";
            rev = "7f64664c872560700d8ea732e40fad1e9b7feae4";
            # sha256 = lib.fakeSha256;
            sha256 = "7DWxFgN0jRqLrL9QR2NZqI+4pM2GJNGc7aBJl4OV+pM=";
          };
        });

      agda = pkgs.agda.withPackages (p: [ agda-stdlib ]);
    in {
      devShells.default =  pkgs.mkShell {
        buildInputs = [
          pkgs.pandoc
          pkgs.marp-cli
          pkgs.R
          pkgs.rPackages.jsonlite
          pkgs.rPackages.rmarkdown
          pkgs.quarto
          agda
        ];
      }; 
    });
}
